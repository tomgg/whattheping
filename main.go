/*
	Software licensing is Too Hard; sue me if you have the time to waste.
*/
package main

import (
	"errors"
	"fmt"
	"image"
	"image/draw"
	"image/png"
	"log"
	"net/http"
	"os"
	"os/exec"
	"sort"
	"strings"
	"sync"
	"time"
)

/*
 * Configure stuff here
 */
var (
	default_station = "IDR663"

	cmd_viewer      = "/usr/bin/feh"
	cmd_viewer_argv = []string{
		"-g1024x1024-1024", "-Z", "-D1", "--keep-zoom-vp",
	}
)

const (
	USAGE = "Usage: wtp [station id]\nUsage: wtp stations"

	FMT_RAIN  = "http://ws.cdn.bom.gov.au/radar/%s.T.%s.png"
	FMT_TRANS = "http://www.bom.gov.au/products/radar_transparencies/%s.%s.png"
	FMT_DATE  = "200601021504"
	FMT_CACHE = "%s/%s-%s.png"
	FMT_SAVED = "%s: saved\n"

	ERR_RAIN_RATE = "warn: no rainfall data for %s\n"
	ERR_GET       = "error: couldn't GET image at %s.\n"
	ERR_DECODE    = "error: couldn't decode %s.\n"
	ERR_OPEN      = "error: couldn't open %s."
)

var (
	rainchasers sync.WaitGroup
)

const (
	BACKGROUND = iota
	TOPOGRAPHY
	RANGE
	LOCATIONS
	NUM_STATION_LAYERS
)

type cache struct {
	dir     string
	query   string
	station [NUM_STATION_LAYERS]*image.Image
	deck    []string
}

func (c *cache) save(data string, rain *image.Image) {
	fname := fmt.Sprintf(FMT_CACHE, c.dir, c.query, data)
	f, err := os.Create(fname)

	defer f.Close()
	if err != nil {
		log.Fatalf(ERR_OPEN, fname)
	}

	slide := image.NewRGBA((*c.station[BACKGROUND]).Bounds())
	b := slide.Bounds()

	draw.Draw(slide, b, *c.station[BACKGROUND], image.ZP, draw.Over)
	draw.Draw(slide, b, *c.station[TOPOGRAPHY], image.ZP, draw.Over)
	draw.Draw(slide, b, *rain, image.ZP, draw.Over)
	draw.Draw(slide, b, *c.station[RANGE], image.ZP, draw.Over)
	draw.Draw(slide, b, *c.station[LOCATIONS], image.ZP, draw.Over)

	png.Encode(f, slide)
}

func (c *cache) cache_slide(ts string, ready chan string) {
	fs := fmt.Sprintf(FMT_CACHE, c.dir, c.query, ts)

	// check if file is cached
	if _, err := os.Stat(fs); err == nil {
		log.Printf("%s: grabbed file from cache", ts)
		ready <- fs
	} else {
		rain, err := get_img(
			fmt.Sprintf(FMT_RAIN, c.query, ts))

		if err != nil {
			log.Printf(ERR_RAIN_RATE, ts)
		} else {
			c.save(ts, rain)
			log.Printf(FMT_SAVED, ts)
			ready <- fs
		}
	}
	rainchasers.Done()
}

func (c *cache) lookup(station, data string) (*image.Image, error) {
	caddr := fmt.Sprintf(FMT_CACHE, c.dir, station, data)

	log.Printf("checking cache for %s\n", caddr)
	if _, err := os.Stat(caddr); err != nil {
		// not cached, fetch it instead
		return nil, err
	}

	f, err := os.Open(caddr)
	if err != nil {
		log.Fatal("Couldn't open cached file")
	}
	img, _, err := image.Decode(f)
	if err != nil {
		log.Fatal("Couldn't decode image")
	}
	f.Close()

	return &img, nil
}

func (c *cache) lookup_station() *cache {
	layers := [...]string{
		"background", "topography", "range", "locations",
	}

	for z, l := range layers {
		i, err := c.lookup(c.query, l)

		if err == nil {
			c.station[z] = i
			continue
		}

		c.station[z], err = get_img(
			fmt.Sprintf(FMT_TRANS, c.query, l))
		if err != nil {
			log.Fatalf("Couldn't get station layer %s\n", l)
		}
		f, err := os.Create(fmt.Sprintf(FMT_CACHE, c.dir, c.query, l))
		if err != nil {
			log.Fatal("Couldn't save file to cache")
		}
		png.Encode(f, *c.station[z])
		f.Close()
	}
	log.Printf("got station %s\n", c.query)
	return c
}

func (c *cache) open() *cache {
	cachedir := os.TempDir() + "/wtp"

	if _, err := os.Stat(cachedir); err != nil {
		if os.MkdirAll(cachedir, 0755) != nil {
			log.Fatal("Couldn't create cache dir")
		}
	}

	if c.query == "" {
		c.query = default_station
	}
	c.dir = cachedir
	c.deck = make([]string, 0, 7)

	return c
}

func (c *cache) lookup_last_hour() *cache {
	now := time.Now().UTC()
	ready := make(chan string)

	// rain data tends to be saved every 6 minutes after the hour.
	now = now.Add(-(time.Duration(now.Minute()) % 6) * time.Minute)

	for i := 0; i < 60; i++ {
		// TODO: how do ~6 workers that each check a 10 minute block
		// perform?
		tstamp := now.Format(FMT_DATE)
		rainchasers.Add(1)
		go c.cache_slide(tstamp, ready)
		now = now.Add(-1 * time.Minute)
	}

	go func() {
		rainchasers.Wait()
		close(ready)
	}()

	for next := range ready {
		c.deck = append(c.deck, next)
	}

	if len(c.deck) < 1 {
		log.Fatal("didn't find any rain data; try a different station.")
	}

	return c
}

func (c *cache) view() {
	sort.Strings(c.deck)
	cmd_viewer_argv = append(cmd_viewer_argv, c.deck...)
	cmd := exec.Command(cmd_viewer, cmd_viewer_argv...)
	cmd.Start()
}

func get_img(url string) (*image.Image, error) {
	res, err := http.Get(url)

	if err != nil {
		return nil, err
	}
	if res.StatusCode != 200 {
		err = errors.New(fmt.Sprintf("Couldn't get image at %s\n", url))
		return nil, err
	}

	img, _, err := image.Decode(res.Body)
	res.Body.Close()
	if err != nil {
		log.Print(ERR_DECODE)
		return nil, err
	}

	return &img, err
}

func wtp_ls(a []string) {
	var STATIONS = map[string]string{
		"NSWACT": `			== NSW & ACT ==
Sydney		Wollongong	Canberra	Yarrawonga
IDR714	64 km	IDR034  64 km	IDR403 128 km	IDR494	64 km
IDR713 128 km	IDR033 128 km	IDR402 256 km	IDR493 128 km
IDR712 256 km	IDR032 256 km	IDR401 512 km	IDR492 256 km
IDR711 512 km	IDR031 512 km			IDR491 512 km

Grafton		Mildura		Moree		Wagga Wagga
IDR283 128 km	IDR303 128 km	IDR533 128 km	IDR553 128 km
IDR282 256 km	IDR302 256 km	IDR532 256 km	IDR552 256 km
IDR281 512 km	IDR301 512 km	IDR531 512 km	IDR551 512 km

Namoi		Newcastle	Norfolk Island
IDR694	64 km	IDR043 128 km	IDR623 128 km
IDR693 128 km	IDR042 256 km	IDR622 256 km
IDR692 256 km	IDR041 512 km	IDR621 512 km
IDR691 512 km`,

		"NT": `			== NT ==
Darwin		Katherine	Gove		Wyndham WA
IDR633 128 km	IDR423 128 km	IDR093 128 km	IDR073 128 km
IDR632 256 km	IDR422 256 km	IDR092 256 km	IDR072 256 km
IDR631 512 km	IDR421 512 km	IDR091 512 km	IDR071 512 km

Alice Springs	Tennant Creek
IDR253 128 km	IDR653 128 km
IDR252 256 km	IDR652 256 km
IDR251 512 km	IDR651 512 km`,

		"QLD": `			== QLD ==
+---------Brisbane-----------+
|Mt Stapylton	Marburg	     |	Bowen		Cairns
|IDR664	64 km	IDR503 128 km|	IDR243 128 km	IDR193 128 km
|IDR663 128 km	IDR502 256 km|	IDR242 256 km	IDR192 256 km
|IDR662 256 km	IDR501 512 km|	IDR241 512 km	IDR191 512 km
|IDR661 512 km		     |
+----------------------------+
Emerald		Gladstone	Gympie		Longreach (pt)
IDR724  64 km	IDR233 128 km	IDR083 128 km	IDR563 128 km
IDR723 128 km	IDR232 256 km	IDR082 256 km	IDR562 256 km
IDR722 256 km	IDR231 512 km	IDR081 512 km	IDR561 512 km
IDR721 512 km

Mackay (pt)	Mornington	Townsville	Warrego
IDR223 128 km	IDR363 128 km	IDR213 128 km	IDR673 128 km
IDR222 256 km	IDR362 256 km	IDR212 256 km	IDR672 256 km
IDR221 512 km	IDR361 512 km	IDR211 512 km	IDR671 512 km

Weipa		Willis Island
IDR183 128 km	IDR413 128 km
IDR182 256 km	IDR412 256 km
IDR181 512 km	IDR411 512 km`,

		"SA": `			== SA ==
+---------Adelaide-----------+
|Buckland Park	Sellicks Hill|	Ceduna (pt)	Eucla (pt) WA
|IDR644	64 km	IDR463 128 km|	IDR333 128 km	IDR453 128 km
|IDR643 128 km	IDR462 256 km|	IDR332 256 km	IDR452 256 km
|IDR642 256 km	IDR461 512 km|	IDR331 512 km	IDR451 512 km
|IDR641 512 km		     |
+----------------------------+
Mildura		Mt Gambier	Woomera
IDR303 128 km	IDR143 128 km	IDR273 128 km
IDR302 256 km	IDR142 256 km	IDR272 256 km
IDR301 512 km	IDR141 512 km	IDR271 512 km`,

		"TAS": `			== TAS ==
Hobart (pt)	N.W. Tas (West Takone)
IDR373 128 km	IDR523 128 km
IDR372 256 km	IDR522 256 km
IDR371 512 km	IDR521 512 km`,

		"VIC": `			== VIC ==
Melbourne	Bairnsdale	Mildura (pt)
IDR024	64 km	IDR683 128 km	IDR303 128 km
IDR023 128 km	IDR682 256 km	IDR302 256 km
IDR022 256 km	IDR681 512 km	IDR301 512 km
IDR021 512 km

Yarrawonga	Mt Gambier (pt) SA
IDR494	64 km	IDR143 128 km
IDR493 128 km	IDR142 256 km
IDR492 256 km	IDR141 512 km
IDR491 512 km`,

		"WA": `			== WA ==
	Perth   Albany  Broome  Dampier  Carnarvon
128 km	IDR703  IDR313  IDR173  IDR153   IDR053
256 km	IDR702  IDR312  IDR172  IDR152   IDR052
512 km	IDR701  IDR311  IDR171  IDR151   IDR051

	Esperance  Eucla   Geraldton  Giles   Halls Creek
128 km	IDR323	   IDR453  IDR063     IDR443  IDR393
256 km	IDR322	   IDR452  IDR062     IDR442  IDR392
512 km	IDR321	   IDR451  IDR061     IDR441  IDR391

	Kalgoorlie  Learmonth  Pt Hedland  Wyndham
128 km	IDR483      IDR293     IDR163     IDR073
256 km	IDR482      IDR292     IDR162     IDR072
512 km	IDR481      IDR291     IDR161     IDR071`,
	}

	if len(a) > 2 {
		for _, c := range a[1:] {
			if s, ok := STATIONS[strings.ToUpper(c)]; ok {
				fmt.Println(s)
			}
		}
	} else {
		var stations []string // go maps should sort themselves

		for k := range STATIONS {
			stations = append(stations, k)
		}
		sort.Strings(stations)
		for _, s := range stations {
			fmt.Println(s)
		}
	}

	os.Exit(0)
}

func main() {
	c := &cache{}

	if len(os.Args) > 1 {
		switch os.Args[1] {
		case "ls":
			wtp_ls(os.Args)
		default:
			c.query = os.Args[1]
		}
	}

	c.open().
		lookup_station().
		lookup_last_hour().
		view()
}
